//
//  ImageComparison.h
//  europeanaedu
//
//  Created by Terry Goodwin on 08/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Tool to compare two photo portraits and give a percentage match
//

#ifndef europeanaedu_ImageComparison_h
#define europeanaedu_ImageComparison_h

#import <UIKit/UIKit.h>

@interface ImageComparison : NSObject {

}

+(float)compareImagesWithLeft:(UIImage*)left andRight:(UIImage*)right;

@end

#endif

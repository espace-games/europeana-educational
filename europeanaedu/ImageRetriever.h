//
//  ImageRetriever.h
//  europeanaedu
//
//  Created by Terry Goodwin on 05/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Holds a list of Europeana items and attempts to retrieve the data for all of them, then holds that data in an array.
//

#ifndef europeanaedu_ImageRetriever_h
#define europeanaedu_ImageRetriever_h

#import "Europeana.h"

@interface ImageRetriever : NSObject {
    EuropeanaRequest* _request;
    NSMutableArray* _images;
}

+(NSArray*)IMAGE_IDS;

-(EuropeanaRequest*)request;
-(NSMutableArray*)images;

-(void)addImage:(EuropeanaObject*)newImage;

-(void)retrieveImages:(EuropeanaObjectCallback)first withSingle:(EuropeanaObjectCallback)single andTotal:(ArrayCallback)total;
-(void)iterateThroughAndRetrieveImagesWithIndex:(int)index withSingle:(EuropeanaObjectCallback)single andTotal:(ArrayCallback)total;
-(void)retrieveImageFromID:(NSString*)imageId withCallback:(EuropeanaObjectCallback)callback;

@end

#endif

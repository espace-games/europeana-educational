//
//  PhotoController.m
//  europeanaedu
//
//  Created by Terry Goodwin on 16/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the "Next Painting" screen. From here we can view the painting info, or proceed to matching
//

#import "PhotoViewController.h"
#import "AppNavigationController.h"

@implementation PhotoViewController

-(IBAction)unwindToPhotoViewController:(UIStoryboardSegue*)segue {
    //nothing goes here
    UIViewController* sourceViewController = (UIViewController*)[segue sourceViewController];
    NSString* identifier = [sourceViewController restorationIdentifier];
    if ([identifier compare:@"editPhotoViewController"] == NSOrderedSame) {
        [self performSelector:@selector(matchButtonPress:) withObject:self afterDelay:1.0];
    }
}

-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender {
    return YES;
}

-(void) viewWillAppear:(BOOL)animated {
    AppNavigationController* parentController = (AppNavigationController*)[self navigationController];
    if (parentController == nil) {
        return;
    }
    
    gameController = [parentController gameController];
    [gameController setCurrentViewController:self];
    
    [navigationTitle setTitle:[gameController navigationTitle]];
    
    EuropeanaObject* currentObject = [gameController currentObject];
    [painting setImage:[currentObject textureData]];
    
    [paintingTitle setText:[currentObject title]];
    [paintingArtist setText:[currentObject credit]];
    [paintingProvider setText:[currentObject provider]];
}

-(IBAction)matchButtonPress:(id)sender {
    [gameController showCameraWithView:self];
}

-(IBAction)infoButtonPress:(id)sender {
    [self performSegueWithIdentifier:@"showInfo" sender:self];
}


@end

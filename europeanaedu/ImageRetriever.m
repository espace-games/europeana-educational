//
//  ImageRetriever.m
//  europeanaedu
//
//  Created by Terry Goodwin on 05/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Holds a list of Europeana items and attempts to retrieve the data for all of them, then holds that data in an array.
//

#import <Foundation/Foundation.h>
#import "ImageRetriever.h"

@implementation ImageRetriever

const NSString* IMAGE_WIDTH = @"640";
const NSString* IMAGE_HEIGHT = @"800";
const NSString* KEEP_ASPECT = @"true";
const NSString* POWER_OF_2 = @"false";
const NSString* CROP_PERCENTAGE = @"10";
const NSString* VERBOSE_LOGGING = @"true";

+(NSArray*) IMAGE_IDS {
    return [[NSArray alloc] initWithObjects: @"/90402/SK_A_190",
                                             @"/90402/SK_A_3262",
                                             @"/90402/SK_A_383",
                                             @"/90402/SK_A_2084",
                                             @"/90402/SK_A_4691",
                                             @"/90402/SK_A_4857",
                                             @"/90402/SK_A_4225",
                                             @"/08559/en_root551__show_image__id_170",
                                             @"/08559/en_root551__show_image__id_120",
                                             @"/9200267/BibliographicResource_3000059120981",
                                             @"/90402/SK_A_4050",
                                             @"/08559/en_root551__show_image__id_161",
                                             @"/08559/en_root551__show_image__id_130",
                                             @"/90402/SK_A_465",
                                             @"/9200267/BibliographicResource_3000059120871",
                                             @"/90402/SK_A_4703",
                                             @"/9200267/BibliographicResource_3000059120905",
                                             nil];
}

-(EuropeanaRequest*)request {
    return _request;
}
-(void)setRequest:(EuropeanaRequest*)newValue {
    _request = newValue;
}

-(NSMutableArray*)images {
    return _images;
}
-(void)setImages:(NSMutableArray*)newValue {
    _images = newValue;
}

-(void)addImage:(EuropeanaObject*)newImage {
    NSLog(@"ImageRetriever addImage");

    bool foundIt = false;
    for (int i = 0; i < [[self images] count]; i++) {
        EuropeanaObject* image = [[self images] objectAtIndex:i];
        if ([[image objectId] caseInsensitiveCompare:[newImage objectId]] == NSOrderedSame) {
            foundIt = true;
            break;
        }
    }
    if (foundIt) {
        return;
    }
    
    [newImage setIndex:[[self images] count]];
    [[self images] addObject:newImage];
}

-(void)retrieveImages:(EuropeanaObjectCallback)first withSingle:(EuropeanaObjectCallback)single andTotal:(ArrayCallback)total {
    NSLog(@"ImageRetriever Retrieving images!");
    
    [self setImages:[[NSMutableArray alloc] init]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self iterateThroughAndRetrieveImagesWithIndex:-1 withSingle:^(EuropeanaObject* image) {
            if ([[self images] count] == 1) {
                if (first != nil) {
                    first(image);
                }
            }
            if (single != nil) {
                single(image);
            }
         } andTotal:^(NSArray* images) {
            NSLog(@"Done getting images! %lu", (unsigned long)[images count]);
            if (total != nil) {
                total(images);
            }
        }];
    });
}

-(void)iterateThroughAndRetrieveImagesWithIndex:(int)index withSingle:(EuropeanaObjectCallback)single andTotal:(ArrayCallback)total {
    index++;
    if (index < 0) {
        index = 0;
    }
    
    NSArray* imageIds = [ImageRetriever IMAGE_IDS];
    
    if (index >= [imageIds count]) {
        NSLog(@"Index exceeds image ids count!");
        if (total != nil) {
            total([self images]);
        }
        return;
    }
    
    
    NSString* imageId = [imageIds objectAtIndex:index];
    [self retrieveImageFromID:imageId withCallback:^(EuropeanaObject* image) {
        if (image != nil) {
            NSLog(@"Got image with id %@", imageId);
            [self addImage:image];

            if (single != nil) {
                single(image);
            }
        } else {
            NSLog(@"Did not get image with id %@", imageId);
        }
        [self iterateThroughAndRetrieveImagesWithIndex:index withSingle:single andTotal:total];
    }];
}

-(void)retrieveImageFromID:(NSString*)imageId withCallback:(EuropeanaObjectCallback)callback {
    
    EuropeanaRequest* newRequest = [Europeana GetImage:[[NSDictionary alloc] initWithObjectsAndKeys:
                                    imageId, [Europeana OPTION_ID],
                                    IMAGE_WIDTH, [Europeana OPTION_WIDTH],
                                    IMAGE_HEIGHT, [Europeana OPTION_HEIGHT],
                                    KEEP_ASPECT, [Europeana OPTION_KEEP_ASPECT],
                                    POWER_OF_2, [Europeana OPTION_POWER_OF_2],
                                    CROP_PERCENTAGE, [Europeana OPTION_CROP_PERCENTAGE],
                                    VERBOSE_LOGGING, [Europeana OPTION_VERBOSE_LOGGING],
                                    [Europeana VALUE_FALSE], [Europeana OPTION_NO_MEDIA],
                                    nil]
    withCallback:^(EuropeanaObject* image) {
        NSLog(@"Retrieved image: %@", [image objectId]);

        if (callback != nil) {
            callback(image);
        }
    } andError:^(int error) {
        NSLog(@"Got an error message! It follows:");
        [Europeana PrintError:error];

        if (callback != nil) {
            callback(nil);
        }
    }];
    
    [self setRequest:newRequest];
}

@end

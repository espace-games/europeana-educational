//
//  GameController.h
//  europeanaedu
//
//  Created by Terry Goodwin on 05/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Controls the flow of the app, hands off to different controllers to perform various tasks and ties everything together.
//

#ifndef europeanaedu_GameController_h
#define europeanaedu_GameController_h

#import <UIKit/UIKit.h>
#import "ImageRetriever.h"
#import "PhotoController.h"
#import "OverlayViewController.h"
#import "PhotoUpdater.h"

@interface GameController : NSObject <PhotoController, UIAlertViewDelegate> {
    UIViewController* rootViewController;
    
    ImageRetriever* imageRetriever;
    OverlayViewController* cameraOverlay;

    UIViewController* testLaunch;
    UIViewController* viewController;
    UIImageView* imageView;
    
    UIImageView* comparisonLeft;
    UIImageView* comparisonRight;
    
    NSMutableArray* images;
    EuropeanaObject* _currentObject;
    
    int imageIndex;
    bool waiting;
    bool gotImages;
    
    UIImage* _userPhoto;
    UIImage* _processedPhoto;
    
    NSObject<PhotoUpdater>* photoUpdater;
}

-(void)setRootViewController:(UIViewController*)newController;
-(void)setNextPaintingAndBeginIfShould;
-(void)setCurrentViewController:(UIViewController*)newController;
-(void)showCameraWithView:(UIViewController*)showController;
-(void)retakePhotoWithView:(UIViewController*)showController;
-(NSString*)navigationTitle;

-(EuropeanaObject*)currentObject;

-(UIImage*)userPhoto;
-(UIImage*)userPhotoCopy;
-(void)setProcessedImage:(UIImage*)processed;
-(UIImage*)processedPhoto;
-(UIImage*)processedPhotoCopy;
-(UIImage*)processedPhotoThumbnail;
-(UIImage*)painting;

-(float)calculateComparisonPercentage;

-(void)setPhotoUpdater:(NSObject<PhotoUpdater>*)updater;
-(void)removePhotoUpdater:(NSObject<PhotoUpdater>*)updater;

@end

#endif

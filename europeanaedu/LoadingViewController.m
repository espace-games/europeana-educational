//
//  Loading.m
//  europeanaedu
//
//  Created by Terry Goodwin on 12/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the initial loading screen, the entry point for the app
//

#import "LoadingViewController.h"
#import "AppNavigationController.h"
#import "AppDelegate.h"

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Loaded loading...");
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    gameController = [appDelegate gameController];
    [gameController setRootViewController:self];
}

-(void)viewDidAppear:(BOOL)animated {
    [gameController setNextPaintingAndBeginIfShould];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowPainting"]) {
        AppNavigationController* navigation = (AppNavigationController*)[segue destinationViewController];
        [navigation setGameController:gameController];
    }
}

-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender {
    return NO;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end

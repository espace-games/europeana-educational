//
//  UINavigationController_AppNavigationController.h
//  europeanaedu
//
//  Created by SGInt_003 on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Keep a reference to the Game Controller here, so anything within the navigation stack can access it
//

#ifndef europeanaedu_AppNavigationController_h
#define europeanaedu_AppNavigationController_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface AppNavigationController : UINavigationController {
    GameController* _gameController;
}

-(void)setGameController:(GameController*)newValue;
-(GameController*)gameController;

@end

#endif
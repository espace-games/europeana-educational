//
//  ResultsViewController.h
//  europeanaedu
//
//  Created by SGInt_003 on 18/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Simple controller to handle the results view
//

#ifndef europeanaedu_ResultsViewController_h
#define europeanaedu_ResultsViewController_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface ResultsViewController : UIViewController {
    IBOutlet UILabel* navigationTitle;
    
    IBOutlet UIImageView* photo;
    IBOutlet UILabel* rating;
    
    UIImage* processedPhoto;
    
    GameController* gameController;
}

@end

#endif

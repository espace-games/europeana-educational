//
//  FilterCollectionView.m
//  europeanaedu
//
//  Created by SGInt_003 on 19/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Collection view controller for the filters, handles filling the grid with items and applying filters to the photos
//

#import "FilterCollectionViewController.h"
#import "FilterCell.h"
#import "AppDelegate.h"
#import "UIImage+Effects.h"

@implementation FilterCollectionViewController

-(void)viewWillAppear:(BOOL)animated {
    [self setFilteredImages];
    [self.collectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    [self addFilterData];

    return [filterNames count];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath {
    FilterCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"filterCell" forIndexPath:indexPath];
    
    NSUInteger length = [indexPath length];
    NSUInteger theIndex = [indexPath indexAtPosition:length-1];

    NSString* name = [filterNames objectAtIndex:theIndex];
    UIImage* image = [filterImages objectAtIndex:theIndex];
    [cell setFilter:name withImage:image];
    
    if ([self indexOfAppliedFilter:name] > -1) {
        NSLog(@"Getting cell for index %lu, filter %@ is already applied", (unsigned long)theIndex, name);
        [cell select];
    } else {
        NSLog(@"Getting cell for index %lu, filter %@ is not applied", (unsigned long)theIndex, name);
        [cell deselect];
    }
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger length = [indexPath length];
    NSUInteger theIndex = [indexPath indexAtPosition:length-1];
    
    NSString* name = [filterNames objectAtIndex:theIndex];
    
    if ([self indexOfAppliedFilter:name] > -1) {
        NSLog(@"Filter %@ is already applied", name);
        [self removeFilter:name];
        return;
    }
    
    NSLog(@"Filter %@ is not applied", name);
    [self applyFilter:name];
}

-(void)applyFilter:(NSString*)filterName {
    int applied = [self indexOfAppliedFilter:filterName];
    if (applied > -1) {
        return;
    }
    
    [filteredNames addObject:filterName];
    
    UIImage* processed = [gameController processedPhoto];
    processed = [self filterImage:processed withFilter:filterName];
    
    [gameController setProcessedImage:processed];
    
    [self setFilteredImages];
    [self.collectionView reloadData];
}

-(void)removeFilter:(NSString*)filterName {
    int applied = [self indexOfAppliedFilter:filterName];
    if (applied <= -1) {
        return;
    }
    
    [filteredNames removeObjectAtIndex:applied];
    
    UIImage* processed = [gameController userPhotoCopy];
    for (int i = 0; i < [filteredNames count]; i++) {
        NSString* filter = [filteredNames objectAtIndex:i];
        NSLog(@"Applying filter %@", filter);
        processed = [self filterImage:processed withFilter:filter];
    }
    
    [gameController setProcessedImage:processed];
    
    [self setFilteredImages];
    [self.collectionView reloadData];
}

-(int)indexOfAppliedFilter:(NSString*)filterName {
    for (int i = 0; i < [filteredNames count]; i++) {
        NSString* name = [filteredNames objectAtIndex:i];
        if ([name compare:filterName] == NSOrderedSame) {
            return i;
        }
    }
    return -1;
}

-(void)addFilterData {
    if (gameController != nil) {
        return;
    }
    
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate == nil) {
        return;
    }
    
    gameController = [appDelegate gameController];
    
    filteredNames = [[NSMutableArray alloc] init];
    
    filterNames = [[NSArray alloc] initWithObjects:@"Lighten", @"Darken", @"Contrast", @"Saturate", @"Sepia", @"Mono", @"Vintage", @"Transfer", @"Noir", nil];

    [self setFilteredImages];
}

-(void)setFilteredImages {
    NSMutableArray* newFiltered = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [filterNames count]; i++) {
        NSString* name = [filterNames objectAtIndex:i];
        UIImage* photo = nil;
        
        int applied = [self indexOfAppliedFilter:name];
        
        if (applied > -1) {
            photo = [filterImages objectAtIndex:i];
        } else {
            photo = [gameController processedPhotoThumbnail];
            photo = [self filterImage:photo withFilter:name];
        }
        
        [newFiltered addObject:photo];
    }
    
    filterImages = newFiltered;
}

-(UIImage*)filterImage:(UIImage*)image withFilter:(NSString*)filterName {
    if ([filterName compare:@"Lighten"] == NSOrderedSame) {
        image = [UIImage lighten:image];
    } else if ([filterName compare:@"Darken"] == NSOrderedSame) {
        image = [UIImage darken:image];
    } else if ([filterName compare:@"Contrast"] == NSOrderedSame) {
        image = [UIImage contrast:image withAmount:2.0];
    } else if ([filterName compare:@"Saturate"] == NSOrderedSame) {
        image = [UIImage saturate:image withFactor:2.0];
    } else if ([filterName compare:@"Sepia"] == NSOrderedSame) {
        image = [UIImage sepia:image];
    } else if ([filterName compare:@"Mono"] == NSOrderedSame) {
        image = [UIImage greyscale:image];
    } else if ([filterName compare:@"Vintage"] == NSOrderedSame) {
        image = [UIImage vintage:image];
    } else if ([filterName compare:@"Transfer"] == NSOrderedSame) {
        image = [UIImage transfer:image];
    } else if ([filterName compare:@"Noir"] == NSOrderedSame) {
        image = [UIImage noir:image];
    }
    
    return image;
}

@end

//
//  UIViewController_PhotoDetails.h
//  europeanaedu
//
//  Created by Terry Goodwin on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the painting info view, showing title, artist, provider and description. Only direction from here is back to the PhotoView
//

#ifndef europeanaedu_PhotoDetailsViewController_h
#define europeanaedu_PhotoDetailsViewController_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface PhotoDetailsViewController : UIViewController {
    IBOutlet UILabel* paintingTitle;
    IBOutlet UILabel* paintingArtist;
    IBOutlet UILabel* paintingProvider;
    IBOutlet UITextView* paintingDescription;
    
    GameController* gameController;
}

@end

#endif
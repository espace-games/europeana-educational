//
//  AppDelegate.m
//  europeanaedu
//
//  Created by SGInt_003 on 01/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  The Game Controller lives here, because the App Delegate can be cleanly referenced from anywhere else so we can access it
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(GameController*)gameController {
    return _gameController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    _gameController = [[GameController alloc] init];
    return YES;
}

@end

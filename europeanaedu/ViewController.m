//
//  ViewController.m
//  europeanaedu
//
//  Created by SGInt_003 on 01/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    gameController = [[GameController alloc] initWithViewController:self andImageView:[self paintingImageView] compLeft:comparisonLeft compRight:comparisonRight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIImageView*)paintingImageView {
    return _paintingImageView;
}
-(void)setPaintingImageView:(UIImageView*)imageView {
    _paintingImageView = imageView;
}


@end

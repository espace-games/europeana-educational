//
//  CompleteEditingViewController.m
//  europeanaedu
//
//  Created by SGInt_003 on 18/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Simple view controller to handle confirming completion of photo matching
//

#import "CompleteEditingViewController.h"
#import "AppNavigationController.h"
#import "GameController.h"

@implementation CompleteEditingViewController

-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender {
    return NO;
}

-(IBAction)completeButtonPress:(id)sender {
    UIStoryboard* storyboard = self.storyboard;
    UINavigationController* navigation = self.navigationController;
    UIViewController* results = [storyboard instantiateViewControllerWithIdentifier:@"resultsViewController"];
    
    [navigation setViewControllers:[NSArray arrayWithObject:results] animated:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    AppNavigationController* parentController = (AppNavigationController*)[self navigationController];
    if (parentController == nil) {
        return;
    }
    
    GameController* gameController = [parentController gameController];
    [gameController setCurrentViewController:self];

    painting.image = [gameController painting];
    photo.image = [gameController processedPhoto];
}

@end
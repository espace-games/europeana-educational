//
//  PhotoController.h
//  europeanaedu
//
//  Created by Terry Goodwin on 09/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the "Next Painting" screen. From here we can view the painting info, or proceed to matching
//

#ifndef europeanaedu_PhotoViewController_h
#define europeanaedu_PhotoViewController_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface PhotoViewController : UIViewController {
    IBOutlet UINavigationItem* navigationTitle;
    
    IBOutlet UIImageView* painting;

    IBOutlet UILabel* paintingTitle;
    IBOutlet UILabel* paintingArtist;
    IBOutlet UILabel* paintingProvider;
    
    GameController* gameController;
}

-(IBAction)unwindToPhotoViewController:(UIStoryboardSegue*)segue;

@end

#endif

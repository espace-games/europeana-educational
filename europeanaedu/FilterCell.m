//
//  FilterCell.m
//  europeanaedu
//
//  Created by SGInt_003 on 19/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  A cell in the filter collection
//

#import "FilterCell.h"

@implementation FilterCell

+(UIColor*)selectedColor {
    UIColor* selected = [[UIColor alloc] initWithRed:(239.0/255.0) green:(186.0/255.0) blue:(31.0/255.0) alpha:1];
    return selected;
}
+(UIColor*)deselectedColor {
    UIColor* deselected = [[UIColor alloc] initWithRed:(92.0/255.0) green:(92.0/255.0) blue:(92./255.0) alpha:1];
    return deselected;
}

-(void)setFilter:(NSString*)name withImage:(UIImage*)image {
    if (!_created) {
        [self create];
    }
    [label setText:name];
    preview.image = image;
}

-(void)create {
    if (_created) {
        return;
    }
    
    UIColor* deselected = [FilterCell deselectedColor];
    
    frame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    frame.backgroundColor = deselected;
    preview = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 60, 60)];
    preview.backgroundColor = deselected;
    preview.contentMode = UIViewContentModeScaleAspectFill;
    preview.layer.masksToBounds = YES;
    [frame addSubview:preview];
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, 64, 29)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:13];

    [self.contentView addSubview:frame];
    [self.contentView addSubview:label];

    _created = true;
}

-(bool)created {
    return _created;
}

-(bool)selected {
    return _selected;
}
-(bool)deselected {
    return !_selected;
}

-(void)select {
    UIColor* selected = [FilterCell selectedColor];
    [frame setBackgroundColor:selected];
    [frame setNeedsDisplay];
    
    _selected = true;
}

-(void)deselect {
    UIColor* deselected = [FilterCell deselectedColor];
    frame.backgroundColor = deselected;
    [frame setNeedsDisplay];
    
    _selected = false;
}

@end
//
//  FilterCell.h
//  europeanaedu
//
//  Created by SGInt_003 on 19/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  A cell in the filter collection
//

#ifndef europeanaedu_FilterCell_h
#define europeanaedu_FilterCell_h

#import <UIKit/UIKit.h>

@interface FilterCell : UICollectionViewCell {
    UIView* frame;
    UIImageView* preview;
    UILabel* label;
    
    UIColor* selectedColor;
    UIColor* deselectedColor;
    
    bool _created;
    bool _selected;
}

+(UIColor*)selectedColor;
+(UIColor*)deselectedColor;

-(bool)created;
-(void)create;
-(void)setFilter:(NSString*)name withImage:(UIImage*)image;
-(bool)selected;
-(bool)deselected;
-(void)select;
-(void)deselect;

@end

#endif

//
//  ViewController.h
//  europeanaedu
//
//  Created by SGInt_003 on 01/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface ViewController : UIViewController {
    GameController* gameController;
    
    IBOutlet UIImageView* _paintingImageView;
    
    IBOutlet UIImageView* comparisonLeft;
    IBOutlet UIImageView* comparisonRight;
}

-(UIImageView*)paintingImageView;
-(void)setPaintingImageView:(UIImageView*)imageView;

@end


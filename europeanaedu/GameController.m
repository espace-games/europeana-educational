//
//  GameController.m
//  europeanaedu
//
//  Created by Terry Goodwin on 05/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Controls the flow of the app, hands off to different controllers to perform various tasks and ties everything together
//

#import <Foundation/Foundation.h>
#import "ImageComparison.h"
#import "GameController.h"
#import "UIImage+Effects.h"
#import "OverlayViewController.h"

@implementation GameController

-(void)appendImageAndStartIfWaiting:(EuropeanaObject*)newImage {
    @synchronized(self) {
        if (images == nil) {
            images = [[NSMutableArray alloc] init];
        }
        
        [images addObject:newImage];
    
        if (waiting) {
            [self proceedWithImage:newImage];
        }
    }
}
-(void)setImages:(NSArray*)newImages {
    @synchronized(self) {
        if (newImages == nil || [newImages count] <= 0) {
            UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Couldn't Retrieve Images"
                                                               message:@"Unable to retrieve images from the Europeana Archive. Please check your internet connection and try again."
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [theAlert show];
            
            
        } else {
            images = [[NSMutableArray alloc] initWithArray:newImages];
            
            gotImages = true;
        }
    }
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self begin];
}

-(EuropeanaObject*)currentObject {
    return _currentObject;
}

-(id)init {
    self = [super init];
    if (self) {
        imageIndex = -1;
        waiting = false;
        gotImages = false;
    }
    return self;
}

-(void)setRootViewController:(UIViewController*)newController {
    rootViewController = newController;
}

-(void)setNextPaintingAndBeginIfShould {
    imageIndex++;
    if (imageIndex <= 0) {
        [self begin];
        return;
    }
    @synchronized(self) {
        if (imageIndex >= [images count]) {
            if (!gotImages) {
                waiting = true;
                return;
            }
            
            // Finished, end here?
            imageIndex = 0;
        }
    }
    
    EuropeanaObject* next = [images objectAtIndex:imageIndex];
    [self proceedWithImage:next];
}

-(void)begin {
    if (imageRetriever == nil) {
        imageRetriever = [[ImageRetriever alloc] init];
    }

    [self retrieveImages];
}

-(void)retrieveImages {
    if (imageRetriever == nil) {
        return;
    }

    waiting = true;
    
    [imageRetriever retrieveImages:^(EuropeanaObject* image) {
        NSLog(@"Got an image");
    } withSingle:^(EuropeanaObject* single) {
        [self appendImageAndStartIfWaiting:single];
    } andTotal:^(NSArray* total) {
        [self setImages:total];
    }];
}

-(void)setCurrentViewController:(UIViewController*)newController {
    viewController = newController;
}

-(void)showCameraWithView:(UIViewController*)showController {
    [self setCurrentViewController:showController];
    [self showCamera];
}

-(void)retakePhotoWithView:(UIViewController*)showController {
    [self showCameraWithView:showController];
}

-(void)showCamera {
    if (viewController == nil) {
        if (rootViewController != nil) {
            viewController = rootViewController;
        }
    }
    
    UIImage* europeanaImage = [self painting];

    cameraOverlay = [[OverlayViewController alloc] initWithNibName:@"CameraOverlay" bundle:nil];
    [cameraOverlay beginWithImage:europeanaImage andPhotoController:self andViewController:viewController];
}

-(void)proceedWithImage:(EuropeanaObject*)image {
    NSLog(@"startWithImage");
    
    @synchronized(self) {
        waiting = false;
    }
    _currentObject = image;

    if (imageIndex == 0) {
        [rootViewController performSegueWithIdentifier:@"ShowPainting" sender:self];
    } else {
        UIStoryboard* storyboard = viewController.storyboard;
        UINavigationController* navigation = viewController.navigationController;
        UIViewController* painting = [storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
        
        [navigation setViewControllers:[NSArray arrayWithObject:painting] animated:YES];
    }
}
-(void)gotPhoto:(UIImage*)photo {
    _userPhoto = photo;
}

-(void)editPhoto {
    NSString* editPhotoViewControllerId = @"editPhotoViewController";
    if ([[viewController restorationIdentifier] compare:editPhotoViewControllerId] == NSOrderedSame) {
        return;
    }
    
    UIStoryboard* storyboard = viewController.storyboard;
    UINavigationController* navigation = viewController.navigationController;
    UIViewController* edit = [storyboard instantiateViewControllerWithIdentifier:editPhotoViewControllerId];
    
    [navigation setViewControllers:[NSArray arrayWithObject:edit] animated:YES];
}

-(NSString*)navigationTitle {
    NSString* newTitle = [NSString stringWithFormat:@"Painting %d", (imageIndex+1)];
    return newTitle;
}

-(UIImage*)userPhoto {
    return _userPhoto;
}
-(UIImage*)userPhotoCopy {
    CGImageRef newCgIm = CGImageCreateCopy(_userPhoto.CGImage);
    UIImage* newImage = [UIImage imageWithCGImage:newCgIm scale:_userPhoto.scale orientation:_userPhoto.imageOrientation];
    return newImage;
}

-(void)setProcessedImage:(UIImage*)processed {
    _processedPhoto = processed;
    
    if (photoUpdater != nil) {
        [photoUpdater updatePhoto:_processedPhoto];
    }
}
-(UIImage*)processedPhoto {
    return _processedPhoto;
}
-(UIImage*)painting {
    return [_currentObject textureData];
}

-(UIImage*)processedPhotoCopy {
    CGImageRef newCgIm = CGImageCreateCopy(_processedPhoto.CGImage);
    UIImage* newImage = [UIImage imageWithCGImage:newCgIm scale:_processedPhoto.scale orientation:_processedPhoto.imageOrientation];
    return newImage;
}

-(UIImage*)processedPhotoThumbnail {
    UIImage* photo = _processedPhoto;
    if (photo == nil) {
        photo = _userPhoto;
    }
    
    float aspect = photo.size.height/photo.size.width;
    
    UIImage* thumbnail = [UIImage resize:photo withSize:CGSizeMake(128, (128*aspect))];
    return thumbnail;
}

-(float)calculateComparisonPercentage {
    return [ImageComparison compareImagesWithLeft:[self painting] andRight:[self processedPhoto]];
}

-(void)setPhotoUpdater:(NSObject<PhotoUpdater>*)updater {
    photoUpdater = updater;
}
-(void)removePhotoUpdater:(NSObject<PhotoUpdater>*)updater {
    if (updater == photoUpdater) {
        photoUpdater = nil;
    }
}

@end
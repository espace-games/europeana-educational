//
//  PhotoDetails.m
//  europeanaedu
//
//  Created by Terry Goodwin on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the painting info view, showing title, artist, provider and description. Only direction from here is back to the PhotoView
//

#import "PhotoDetailsViewController.h"
#import "AppNavigationController.h"
#import "GameController.h"
#import "Europeana.h"

@implementation PhotoDetailsViewController

-(void) viewWillAppear:(BOOL)animated {
    AppNavigationController* parentController = (AppNavigationController*)[self navigationController];
    if (parentController == nil) {
        return;
    }
    
    gameController = [parentController gameController];

    EuropeanaObject* currentObject = [gameController currentObject];
    [paintingTitle setText:[currentObject title]];
    [paintingArtist setText:[currentObject credit]];
    [paintingProvider setText:[currentObject provider]];
    [paintingDescription setText:[currentObject description]];
}

@end
//
//  UIViewController_OverlayViewController.h
//  europeanaedu
//
//  Created by Terry Goodwin on 09/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  The controller for the custom camera overlay. Handles taking photos and scaling the resultant image appropriately
//

#ifndef europeanaedu_OverlayViewController_h
#define europeanaedu_OverlayViewController_h

#import <UIKit/UIKit.h>
#import "PhotoController.h"

@interface OverlayViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    IBOutlet UIImageView* imageView;
    IBOutlet UISlider* opacitySlider;
    IBOutlet UIView* takePhotoButton;
    IBOutlet UIButton* buttonInner;
    
    IBOutlet UIView* bottomPanel;
    IBOutlet UIView* topPanel;
    
    UIImage* testImage;
    UIImageView* pretendCameraView;
    
    UIImagePickerController* photoPicker;
    NSObject<PhotoController>* photoController;
    UIViewController* viewController;
    UIImage* image;
    
    CGAffineTransform cameraTranslation;
    CGAffineTransform cameraScale;
    
    bool styled;
    bool simulator;
    bool rearCamera;
    
    UIInterfaceOrientation deviceOrientation;
}

-(void)setImage:(UIImage*)newImage;
-(void)setPhotoController:(NSObject<PhotoController>*)newController;
-(void)setViewController:(UIViewController*)newViewController;

-(void)setOverlayProperties;

-(void)beginWithImage:(UIImage*)newImage andPhotoController:(NSObject<PhotoController>*)newController andViewController:(UIViewController*)newViewController;

-(IBAction)opacityChanged:(id)sender;
-(IBAction)changeCamera:(id)sender;

@end

#endif

//
//  ResultsViewController.m
//  europeanaedu
//
//  Created by SGInt_003 on 18/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Simple controller to handle the results view
//

#import "ResultsViewController.h"
#import "AppNavigationController.h"

@implementation ResultsViewController

-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender {
    return NO;
}

-(void)viewWillAppear:(BOOL)animated {
    AppNavigationController* parentController = (AppNavigationController*)[self navigationController];
    if (parentController == nil) {
        return;
    }
    
    gameController = [parentController gameController];
    [gameController setCurrentViewController:self];

    [navigationTitle setText:[gameController navigationTitle]];
    
    processedPhoto = [gameController processedPhoto];
    photo.image = processedPhoto;
    
    int ratingPercentage = (int)[gameController calculateComparisonPercentage];
    [rating setText:[NSString stringWithFormat:@"%d%%", ratingPercentage]];
}

-(IBAction)nextButtonPress:(id)sender {
    [gameController setNextPaintingAndBeginIfShould];
}

@end
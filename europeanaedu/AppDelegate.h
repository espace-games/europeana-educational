//
//  AppDelegate.h
//  europeanaedu
//
//  Created by SGInt_003 on 01/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  The Game Controller lives here, because the App Delegate can be cleanly referenced from anywhere else so we can access it
//

#ifndef europeanaedu_AppDelegate_h
#define europeanaedu_AppDelegate_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    GameController* _gameController;
}

@property (strong, nonatomic) UIWindow *window;

-(GameController*)gameController;

@end

#endif
//
//  UIViewController_Loading.h
//  europeanaedu
//
//  Created by Terry Goodwin on 12/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  View controller for the initial loading screen, the entry point for the app
//

#ifndef europeanaedu_LoadingViewController_h
#define europeanaedu_LoadingViewController_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface LoadingViewController : UIViewController {
    GameController* gameController;
}

@end

#endif
//
//  FilterCollectionView.h
//  europeanaedu
//
//  Created by SGInt_003 on 19/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Collection view controller for the filters, handles filling the grid with items and applying filters to the photos
//

#ifndef europeanaedu_FilterCollectionView_h
#define europeanaedu_FilterCollectionView_h

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface FilterCollectionViewController : UICollectionViewController {
    NSArray* filterNames;
    NSMutableArray* filterImages;
    
    NSMutableArray* filteredNames;
    
    GameController* gameController;
    
    bool reloading;
}

-(void)addFilterData;
-(void)applyFilter:(NSString*)filterName;

@end

#endif

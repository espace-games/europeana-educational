//
//  PhotoUpdater.h
//  europeanaedu
//
//  Created by Terry Goodwin on 19/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Protocol to handle updating photos so we don't get tangled up with recursive includes with the EditViewController
//

#ifndef europeanaedu_PhotoUpdater_h
#define europeanaedu_PhotoUpdater_h

@protocol PhotoUpdater <NSObject>

-(void)updatePhoto:(UIImage*)newPhoto;

@end

#endif
//
//  EditPhotoController.m
//  europeanaedu
//
//  Created by Terry Goodwin on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Controller for photo editing / filters
//

#import "EditPhotoViewController.h"
#import "AppNavigationController.h"
#import "UIImage+Effects.h"
#import "FilterCell.h"

@implementation EditPhotoViewController

-(IBAction)unwindToEditPhotoController:(UIStoryboardSegue*)segue {
    //nothing goes here
    NSLog(@"Unwinding to edit photo!");
}

-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated {
    AppNavigationController* parentController = (AppNavigationController*)[self navigationController];
    if (parentController == nil) {
        return;
    }
    
    gameController = [parentController gameController];
    [gameController setCurrentViewController:self];
    [gameController setPhotoUpdater:self];

    painting.image = [gameController painting];

    originalImage = [gameController userPhoto];
    processedImage = [self copyImage:originalImage];
    [gameController setProcessedImage:processedImage];
    [self updatePhoto:processedImage];
}

-(void)viewWillDisappear:(BOOL)animated {
    [gameController removePhotoUpdater:self];
}

-(UIImage*)copyImage:(UIImage*)image {
    CGImageRef newCgIm = CGImageCreateCopy(image.CGImage);
    UIImage* newImage = [UIImage imageWithCGImage:newCgIm scale:image.scale orientation:image.imageOrientation];
    return newImage;
}

-(IBAction)retakeButtonPress:(id)sender {
    [gameController retakePhotoWithView:self];
}

-(IBAction)applyButtonPress:(id)sender {
    [self performSegueWithIdentifier:@"showCompleteEditing" sender:self];
}

-(IBAction)swapButtonPress:(id)sender {
    float alpha = painting.alpha;
    if (alpha == 0.0) {
        painting.alpha = 1.0;
    } else {
        painting.alpha = 0.0;
    }
}

-(void)updatePhoto:(UIImage*)newPhoto {
    processedImage = newPhoto;
    photo.image = processedImage;
}


@end
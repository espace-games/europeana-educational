//
//  PhotoController.h
//  europeanaedu
//
//  Created by Terry Goodwin on 09/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Protocol to handle getting and editing photos so we don't get tangled up with recursive includes with the GameController
//

#ifndef europeanaedu_PhotoController_h
#define europeanaedu_PhotoController_h

#import <UIKit/UIKit.h>

@protocol PhotoController <NSObject>

-(void)gotPhoto:(UIImage*)photo;
-(void)editPhoto;

@end

#endif
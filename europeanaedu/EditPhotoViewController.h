//
//  EditPhotoController.h
//  europeanaedu
//
//  Created by Terry Goodwin on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Controller for photo editing / filters
//

#ifndef europeanaedu_EditPhotoController_h
#define europeanaedu_EditPhotoController_h

#import <UIKit/UIKit.h>
#import "GameController.h"
#import "FilterCollectionViewController.h"
#import "PhotoUpdater.h"

@interface EditPhotoViewController : UIViewController <PhotoUpdater> {
    IBOutlet UIImageView* photo;
    IBOutlet UIImageView* painting;
    IBOutlet FilterCollectionViewController* collection;
    
    UIImage* originalImage;
    UIImage* processedImage;
    
    NSMutableArray* filters;
    
    GameController* gameController;
    
    UIColor* selectedColor;
    UIColor* standardColor;
    
}

-(IBAction)unwindToEditPhotoController:(UIStoryboardSegue*)segue;

@end

#endif

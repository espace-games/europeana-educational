//
//  OverlayViewController.m
//  europeanaedu
//
//  Created by Terry Goodwin on 09/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  The controller for the custom camera overlay. Handles taking photos and scaling the resultant image appropriately
//

#import <Foundation/Foundation.h>
#import "OverlayViewController.h"
#import "UIImage+Effects.h"

@implementation OverlayViewController

-(void)setImage:(UIImage*)newImage {
    image = newImage;
}

-(void)setPhotoController:(NSObject<PhotoController>*)newController {
    photoController = newController;
}
-(void)setViewController:(UIViewController*)newViewController {
    viewController = newViewController;
}

-(void)beginWithImage:(UIImage*)newImage andPhotoController:(NSObject<PhotoController>*)newController andViewController:(UIViewController*)newViewController {
    if (photoPicker != nil) {
        return;
    }
    
    [self setPhotoController:newController];
    [self setViewController:newViewController];
    [self setImage:newImage];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        simulator = true;
        testImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"testb.jpg" ofType:nil]];

        pretendCameraView = [[UIImageView alloc] initWithFrame:imageView.frame];
        pretendCameraView.image = testImage;
         
        [[self view] insertSubview:pretendCameraView atIndex:0];

        [viewController presentViewController:self animated:YES completion:nil];
        return;
    }
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    float scale = screenSize.height / screenSize.width*3/4;
    
    cameraTranslation = CGAffineTransformMakeTranslation(0,(screenSize.height - screenSize.width*4/3)*0.5);
    cameraScale = CGAffineTransformMakeScale(scale, scale);
    
    photoPicker = [[UIImagePickerController alloc] init];
    photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    photoPicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    photoPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    photoPicker.allowsEditing = NO;
    photoPicker.showsCameraControls = NO;
    photoPicker.cameraViewTransform = CGAffineTransformConcat(cameraScale, cameraTranslation);
    photoPicker.delegate = self;
    photoPicker.cameraOverlayView = self.view;
    
    [viewController presentViewController:photoPicker animated:YES completion:NULL];
}

-(void)viewWillAppear:(BOOL)animated {
    if (simulator) {
        [self setOverlayProperties];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    if (simulator) {
        CGRect imageViewFrame = imageView.frame;
        [pretendCameraView setFrame:imageViewFrame];
    }
}

-(void)setOverlayProperties {
    if (styled) {
        return;
    }
    
    styled = true;
    
    if (image != nil) {
        imageView.image = image;
    }
    
    takePhotoButton.layer.cornerRadius = ([takePhotoButton frame].size.width/2)-1;
    buttonInner.layer.cornerRadius = ([buttonInner frame].size.width/2)-1;
    buttonInner.layer.borderWidth = 2;
    buttonInner.layer.borderColor = [UIColor blackColor].CGColor;
}


-(IBAction)takePicture:(id)sender {
    [self setCameraButtonInactive];
    [photoPicker takePicture];
    
    deviceOrientation = (UIInterfaceOrientation)[[UIDevice currentDevice] orientation];
    
    if (simulator) {
        [self simulateImageTaken];
    }
}
-(IBAction)startTouchButton:(id)sender {
    [self setCameraButtonActive];
}
-(IBAction)endTouchButton:(id)sender {
    [self setCameraButtonInactive];
}

-(void)setCameraButtonActive {
    [buttonInner setBackgroundColor:[UIColor redColor]];
}
-(void)setCameraButtonInactive {
    [buttonInner setBackgroundColor:[UIColor whiteColor]];
}

-(void)simulateImageTaken {
    [self gotImage:testImage];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage* chosenImage = info[UIImagePickerControllerOriginalImage];
    bool switchSize = false;
    
//    UIInterfaceOrientation deviceOrientation = (UIInterfaceOrientation)[[UIDevice currentDevice] orientation];
    UIImageOrientation newOrientation = UIImageOrientationUpMirrored;
    
    UIImageOrientation orientation = chosenImage.imageOrientation;
    
    if (!rearCamera) {
        if (deviceOrientation == UIInterfaceOrientationLandscapeLeft) {
            newOrientation = UIImageOrientationDownMirrored;
        } else if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            newOrientation = UIImageOrientationRightMirrored;
            switchSize = true;
        } else if (deviceOrientation == UIInterfaceOrientationLandscapeRight) {
            newOrientation = UIImageOrientationUpMirrored;
        } else if (deviceOrientation == UIInterfaceOrientationPortrait) {
            newOrientation = UIImageOrientationLeftMirrored;
            switchSize = true;
        } else if (deviceOrientation == UIDeviceOrientationFaceUp) {
            newOrientation = UIImageOrientationLeftMirrored;
            switchSize = true;
        }
    } else {
        if (deviceOrientation == UIInterfaceOrientationLandscapeLeft) {
            newOrientation = UIImageOrientationUp; //UIImageOrientationDown;
        } else if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            newOrientation = UIImageOrientationLeft; //UIImageOrientationRight;
            switchSize = true;
        } else if (deviceOrientation == UIInterfaceOrientationLandscapeRight) {
            newOrientation = UIImageOrientationDown; //UIImageOrientationUp;
        } else if (deviceOrientation == UIInterfaceOrientationPortrait) {
            newOrientation = UIImageOrientationRight; //UIImageOrientationLeft;
            switchSize = true;
        } else if (deviceOrientation == UIDeviceOrientationFaceUp) {
            newOrientation = UIImageOrientationRight; //UIImageOrientationLeft;
            switchSize = true;
        }
    }

    UIImage* processedImage = [UIImage imageWithCGImage:chosenImage.CGImage scale:chosenImage.scale orientation:newOrientation];
    orientation = chosenImage.imageOrientation;
    processedImage = [UIImage stripOrientation:processedImage];
    orientation = chosenImage.imageOrientation;
    
    CGSize sourceSize = processedImage.size;
    
    if (switchSize) {
        CGSize newSize = sourceSize;
        newSize.width = sourceSize.height;
        newSize.height = sourceSize.width;
    }
    
    CGSize ourSize = image.size;
    
    int newWidth = sourceSize.width/cameraScale.a;
    int newHeight = (sourceSize.height/cameraScale.d);
    int widthDiff = fabs(sourceSize.width-newWidth);
    int heightDiff = fabs(sourceSize.height-newHeight);

    int cropX = (widthDiff/2)-cameraTranslation.tx;
    int cropY = (heightDiff/2)-cameraTranslation.ty;
    
    CGRect topFrame = [topPanel frame];
    if (topFrame.origin.y >= 0) {
        if (!rearCamera) {
            cropY -= ((topFrame.origin.y+topFrame.size.height)/cameraScale.d)*2;
        } else {
            cropY = ((topFrame.origin.y+topFrame.size.height)/cameraScale.d)*2;
        }
    }

    CGRect cropFrame = CGRectMake(cropX, cropY, newWidth, newHeight);
    processedImage = [UIImage crop:processedImage withRect:cropFrame];

    CGSize croppedSize = processedImage.size;
    
    float aspect = croppedSize.width/croppedSize.height;
    processedImage = [UIImage resize:processedImage withSize:CGSizeMake(ourSize.width, ourSize.width/aspect)];
    CGSize resizedSize = processedImage.size;
    
    cropFrame = CGRectMake(0, resizedSize.height-ourSize.height, ourSize.width, ourSize.height);
    processedImage = [UIImage crop:processedImage withRect:cropFrame];
    processedImage = [processedImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];

    [self gotImage:processedImage];
}

-(void)gotImage:(UIImage*)processedImage {
    [photoController gotPhoto:processedImage];
    if (simulator) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [photoController editPhoto];
    } else {
        [photoPicker dismissViewControllerAnimated:YES completion:^(void) {
            [photoController editPhoto];
        }];
    }
}

- (void)navigationController:(UINavigationController*)navigationController didShowViewController:(UIViewController*)checkViewController animated:(BOOL)animated
{
    if ([navigationController isEqual:photoPicker]) {
        [self setOverlayProperties];
    }
}

-(IBAction)opacityChanged:(id)sender {
    float newOpacity = [opacitySlider value];
    imageView.alpha = newOpacity;
}

-(IBAction)changeCamera:(id)sender {
    if (photoPicker.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        photoPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        rearCamera = false;
    }
    else {
        photoPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        rearCamera = true;
    }
}

@end

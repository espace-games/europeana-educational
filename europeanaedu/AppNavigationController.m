//
//  AppNavigationController.m
//  europeanaedu
//
//  Created by SGInt_003 on 17/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Keep a reference to the Game Controller here, so anything within the navigation stack can access it
//

#import "AppNavigationController.h"
#import "PhotoViewController.h"

@implementation AppNavigationController

-(void)setGameController:(GameController*)newValue {
    _gameController = newValue;
}
-(GameController*)gameController {
    return _gameController;
}

@end

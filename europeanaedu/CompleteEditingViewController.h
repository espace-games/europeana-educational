//
//  CompleteEditingViewController.h
//  europeanaedu
//
//  Created by SGInt_003 on 18/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Simple view controller to handle confirming completion of photo matching
//

#ifndef europeanaedu_CompleteEditingViewController_h
#define europeanaedu_CompleteEditingViewController_h

#import <UIKit/UIKit.h>

@interface CompleteEditingViewController : UIViewController {
    IBOutlet UIImageView* painting;
    IBOutlet UIImageView* photo;
}

@end

#endif

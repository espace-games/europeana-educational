//
//  ImageComparison.m
//  europeanaedu
//
//  Created by Terry Goodwin on 08/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//
//  Tool to compare two photo portraits and give a percentage match
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "Europeana.h"
#import "UIColor+HSVAdditions.h"
#import "ImageComparison.h"

@implementation ImageComparison

+(UIColor*)getRGBAsFromImage:(UIImage*)image atX:(int)x andY:(int)y
{
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
    CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
    CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
    CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
    CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
    byteIndex += bytesPerPixel;
    
    UIColor* acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    
    return acolor;
}

+(UIImage*)resizeImage:(UIImage*)source withWidth:(int)width andHeight:(int)height {
    CGSize imageSize = CGSizeMake(width, height);

    UIGraphicsBeginImageContext(imageSize);

    CGRect rect = CGRectMake(0.0, 0.0, width, height);
    
    [source drawInRect:rect];

    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    resizedImage = [resizedImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];

    UIGraphicsEndImageContext();
    
    return resizedImage;
}

+(NSDictionary*)getFacialFeaturesFromImage:(UIImage*)image {
    NSLog(@"getFacialFeaturesFromImage");
    
    NSMutableDictionary* features = [[NSMutableDictionary alloc] init];

    CIDetector* detector = [CIDetector detectorOfType:CIDetectorTypeFace
                                              context:nil
                                              options:[NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy]];
    
    CIImage* CGImage = [CIImage imageWithCGImage:image.CGImage];
    NSArray* featuresArray = [detector featuresInImage:CGImage];
    
    CGFloat faceWidth = -1;
    
    for(CIFaceFeature* faceFeature in featuresArray) {
        if (faceWidth <= -1) {
            faceWidth = faceFeature.bounds.size.width;
            [features setObject:@(faceWidth) forKey:@"faceWidth"];
        }
        
        if(faceFeature.hasLeftEyePosition) {
            CGPoint leftEye = faceFeature.leftEyePosition;
            NSLog(@"Got left eye, x: %f y: %f", leftEye.x, leftEye.y);
            [features setObject:[NSValue valueWithCGPoint:leftEye] forKey:@"leftEye"];
        }
        if(faceFeature.hasRightEyePosition) {
            CGPoint rightEye = faceFeature.rightEyePosition;
            NSLog(@"Got right eye, x: %f y: %f", rightEye.x, rightEye.y);
            [features setObject:[NSValue valueWithCGPoint:rightEye] forKey:@"rightEye"];
        }
        if(faceFeature.hasMouthPosition) {
            CGPoint mouth = faceFeature.mouthPosition;
            NSLog(@"Got mouth, x: %f y: %f", mouth.x, mouth.y);
            [features setObject:[NSValue valueWithCGPoint:mouth] forKey:@"mouth"];
        }
    }
    
    NSDictionary* result = [[NSDictionary alloc] initWithDictionary:features];
    return result;
}

+(float)compareFacialFeaturesWithLeft:(UIImage*)left andRight:(UIImage*)right {
    NSDictionary* leftFeatures = [ImageComparison getFacialFeaturesFromImage:left];
    NSDictionary* rightFeatures = [ImageComparison getFacialFeaturesFromImage:right];

    CGPoint left_leftEye = CGPointMake(-1, -1);
    CGPoint left_rightEye = CGPointMake(-1, -1);
    CGPoint left_mouth = CGPointMake(-1, -1);

    CGPoint right_leftEye = CGPointMake(-1, -1);
    CGPoint right_rightEye = CGPointMake(-1, -1);
    CGPoint right_mouth = CGPointMake(-1, -1);

    bool foundLeftEyeSource = false;
    bool foundRightEyeSource = false;
    bool foundMouthSource = false;
    
    int numFound = 0;
    
    @try {
        left_leftEye = [[leftFeatures objectForKey:@"leftEye"] CGPointValue];
        foundLeftEyeSource = true;
        numFound++;
    } @catch (NSException *exception) {
        NSLog(@"Didn't find left eye in source");
    }
    @try {
        left_rightEye = [[leftFeatures objectForKey:@"rightEye"] CGPointValue];
        foundRightEyeSource = true;
        numFound++;
    } @catch (NSException *exception) {
        NSLog(@"Didn't find right eye in source");
    }
    @try {
        left_mouth = [[leftFeatures objectForKey:@"mouth"] CGPointValue];
        foundMouthSource = true;
        numFound++;
    } @catch (NSException *exception) {
        NSLog(@"Didn't find mouth in source");
    }
    
    if (numFound <= 0) {
        return 100.0;
    }
    
    bool foundLeftEyeSelf = false;
    bool foundRightEyeSelf = false;
    bool foundMouthSelf = false;
    
    int missing = 0;
    if (foundLeftEyeSource) {
        @try {
            right_leftEye = [[rightFeatures objectForKey:@"leftEye"] CGPointValue];
            if (right_leftEye.x > 0 && right_leftEye.y > 0) {
                foundLeftEyeSelf = true;
            } else {
                missing++;
            }
        } @catch (NSException *exception) {
            NSLog(@"Didn't find left eye in selfie");
            missing++;
        }
    }
    if (foundRightEyeSource) {
        @try {
            right_rightEye = [[rightFeatures objectForKey:@"rightEye"] CGPointValue];
            if (right_rightEye.x > 0 && right_rightEye.y > 0) {
                foundRightEyeSelf = true;
            } else {
                missing++;
            }
        } @catch (NSException *exception) {
            NSLog(@"Didn't find right eye in selfie");
            missing++;
        }
    }
    if (foundMouthSource) {
        @try {
            right_mouth = [[rightFeatures objectForKey:@"mouth"] CGPointValue];
            if (right_mouth.x > 0 && right_mouth.y > 0) {
                foundMouthSelf = true;
            } else {
                missing++;
            }
        } @catch (NSException *exception) {
            NSLog(@"Didn't find mouth in selfie");
            missing++;
        }
    }

    NSLog(@"Left features, leftEyeX: %f leftEyeY: %f rightEyeX: %f rightEyeY: %f mouthX: %f mouthY: %f", left_leftEye.x, left_leftEye.y, left_rightEye.x, left_rightEye.y, left_mouth.x, left_mouth.y);
    NSLog(@"Right features, leftEyeX: %f leftEyeY: %f rightEyeX: %f rightEyeY: %f mouthX: %f mouthY: %f", right_leftEye.x, right_leftEye.y, right_rightEye.x, right_rightEye.y, right_mouth.x, right_mouth.y);

    float leftEyeDiff = 0.0;
    float rightEyeDiff = 0.0;
    float mouthDiff = 0.0;
    numFound = 0;
    
    if (foundLeftEyeSelf) {
        leftEyeDiff = (fabs(left_leftEye.x-right_leftEye.x)+fabs(left_leftEye.y-right_leftEye.y))/2.0;
        numFound++;
    }
    if (foundRightEyeSelf) {
        rightEyeDiff = (fabs(left_rightEye.x-right_rightEye.x)+fabs(left_rightEye.y-right_rightEye.y))/2.0;
        numFound++;
    }
    if (foundMouthSelf) {
        mouthDiff = (fabs(left_mouth.x-right_mouth.x)+fabs(left_mouth.y-right_mouth.y))/2.0;
        numFound++;
    }
    
    if (numFound <= 0) {
        return 0.0;
    }
    
    float averageDiff = (leftEyeDiff+rightEyeDiff+mouthDiff)/(float)numFound;
    if (missing > 0) {
        averageDiff *= (((float)missing+1.0)/0.5);
    }

    NSLog(@"Average difference: %f", averageDiff);

    if (averageDiff > 100.0) {
        averageDiff = 100.0;
    } else if (averageDiff < 0.0) {
        averageDiff = 0.0;
    }
    
    averageDiff = 100.0-averageDiff;
    
    return averageDiff;
}

+(float)compareImagesWithLeft:(UIImage*)left andRight:(UIImage*)right {
    float faceScore = [ImageComparison compareFacialFeaturesWithLeft:left andRight:right];
    
    int size = 8;
    
    NSLog(@"compareImagesWithLeft resizing left");
    UIImage* resizedLeft = [ImageComparison resizeImage:left withWidth:size andHeight:size];
    NSLog(@"compareImagesWithLeft resizing right");
    UIImage* resizedRight = [ImageComparison resizeImage:right withWidth:size andHeight:size];
    
    CGFloat totalHueDiff = 0;
    CGFloat totalSaturationDiff = 0;
    CGFloat totalBrightnessDiff = 0;
    
    NSLog(@"compareImagesWithLeft checking hue/saturation/brightness");
    for (int v = 0; v < size; v++) {
        for (int h = 0; h < size; h++) {
            UIColor* leftColor = [ImageComparison getRGBAsFromImage:resizedLeft atX:h andY:v];
            UIColor* rightColor = [ImageComparison getRGBAsFromImage:resizedRight atX:h andY:v];
            
            NSLog(@"Left h: %d v: %d hue: %f saturation: %f brightness: %f", h, v, [leftColor hue], [leftColor saturation], [leftColor brightness]);
            NSLog(@"Right h: %d v: %d hue: %f saturation: %f brightness: %f", h, v, [rightColor hue], [rightColor saturation], [rightColor brightness]);
            
            CGFloat hueDiff = fabs([leftColor hue]-[rightColor hue]);
            CGFloat saturationDiff = fabs([leftColor saturation]-[rightColor saturation]);
            CGFloat brightnessDiff = fabs([leftColor brightness]-[rightColor brightness]);
            
            NSLog(@"Diffs hue: %f saturation: %f brightness: %f", hueDiff, saturationDiff, brightnessDiff);
            
            totalHueDiff += hueDiff;
            totalSaturationDiff += saturationDiff;
            totalBrightnessDiff += brightnessDiff;
        }
    }
    
    float numElements = (float)size*(float)size;
    
    float averageHueDiff = totalHueDiff/numElements;
    float averageSaturationDiff = totalSaturationDiff/numElements;
    float averageBrightnessDiff = totalBrightnessDiff/numElements;
    
    CGFloat totalDiff = (averageHueDiff*1.5)+averageSaturationDiff+(averageBrightnessDiff*0.5); // Weight towards hue, away from brightness
    float averageDiff = (totalDiff/3.0)*100.0;
    
    float finalDiff = ((averageDiff*0.5)+(faceScore*1.5))/2.0; // Weight towards the face score
    
    NSLog(@"Total diffs hue: %f saturation: %f brightness: %f average: %f final: %f", averageHueDiff, averageSaturationDiff, averageBrightnessDiff, averageDiff, finalDiff);
    
    return finalDiff;
}

@end

